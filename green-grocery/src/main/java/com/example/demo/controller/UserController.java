package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Controller
public class UserController {
	 @Autowired
	    private UserRepository repo;
	     
	    @GetMapping("/")
	    public String viewHomePage() {
	        return "index";
	    }

	    @GetMapping("/register")
	    public String showRegistrationForm(Model model) {
	        model.addAttribute("user", new User());
	         
	        return "signup_form";
	    }
	    @PostMapping("/process_register")
	    public String processRegistration(User user) {
	    	 BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	    	    String encodedPassword = passwordEncoder.encode(user.getPassword());
	    	    user.setPassword(encodedPassword);
	    	       
	       repo.save(user);
	         
	        return "register_success";
	    }

	    @GetMapping("/home")
	    public String home(Model model) {
	      List<User>home=repo.findAll()  ;      
	      model.addAttribute("home", home);
	      return "home";
	    } 

	    @GetMapping("/about")
	    public String about() {
	        return "about";
	    }   
	  
	    @GetMapping("/contact")
	    public String contact() {
	        return "contact";
	    }   
	     

	    @GetMapping("/feedback")
	    public String feedback() {
	        return "feedback";
	    }   
	     

	    @GetMapping("/myproducts")
	    public String myproducts() {
	        return "myproducts";
	    }   
	     
	    @GetMapping("/features")
	    public String features() {
	        return "features";
	    }   
	     
	  
	    
	    @GetMapping("/addcart")
	    public String   addcart() {
	    	return "addcart";
	    }   
	    
	    @GetMapping("/paymentform")
	    public String   paymentform() {
	    	return "paymentform";
	    }   
	    
	    @GetMapping("/product")
	    public String   product() {
	    	return "product";
	    }   
	    
	    
	   
	  


} 

	     	


