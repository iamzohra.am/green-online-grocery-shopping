package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import com.example.demo.model.Products;

import com.example.demo.repository.ProductRepository;


@Controller
public class AdminPController {
	@Autowired
    private ProductRepository productRepo;

	@GetMapping("/p")
    public String viewHomePage(Model m) {
       List<Products>list=productRepo.findAll();
		m.addAttribute("all_products", list);
       return "product";
    }

	@GetMapping("/load_form")
    public String loadForm() {
        return "add";
    }
	@GetMapping("/edit_form/{id}")
    public String editForm(@PathVariable (value="id")long id,Model m) {
	Optional<Products> products=productRepo.findById(id);
		Products pro=products.get();
		m.addAttribute("product", pro);
		return "edit";
    }
     @PostMapping("/save_products")
	 public String saveProducts(@ModelAttribute Products products,HttpSession session) {
		productRepo.save(products);
		session.setAttribute("msg", "Product Added Successfully..");
    	 return "redirect:/load_form";
		 
	 }
     @PostMapping("/update_products")
	 public String updateProducts(@ModelAttribute Products products,HttpSession session) {
		productRepo.save(products);
		session.setAttribute("msg", "Product Updated Successfully..");
    	 return "redirect:/p";
		 
	 }
      @GetMapping("/delete/{id}")
     public String deleteProducts(@PathVariable (value="id")long id,HttpSession session)
     {
    		productRepo.deleteById(id);
    		session.setAttribute("msg", "Product Delete Successfully..");
    	  return "redirect:/p";
    	  
     }
      
}
