package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.example.demo.model.Products;
import com.example.demo.repository.ProductRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(value=false)
public class ProductsTest {
	@Autowired
    private ProductRepository repo;
    
    @Mock
    Products products;
    
    @Test
    public void testCreateProduct() {
         Products products = new Products();
            products.setId((long) 1);
            products.setProductName("amul");
            products.setDescription("icecream");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct1() {
         Products products = new Products();
            products.setId((long) 1);
            products.setProductName("amul1");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    

    @Test
    public void testCreateProduct2() {
         Products products = new Products();
            products.setId((long) 2);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct3() {
         Products products = new Products();
            products.setId((long) 3);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct4() {
         Products products = new Products();
            products.setId((long) 4);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct5() {
         Products products = new Products();
            products.setId((long) 5);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct6() {
         Products products = new Products();
            products.setId((long) 6);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    @Test
    public void testCreateProduct7() {
         Products products = new Products();
            products.setId((long) 7);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct8() {
         Products products = new Products();
            products.setId((long) 8);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
    @Test
    public void testCreateProduct9() {
         Products products = new Products();
            products.setId((long) 9);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    @Test
    public void testCreateProduct10() {
         Products products = new Products();
            products.setId((long) 10);
            products.setProductName("amul2");
            products.setDescription("icecream1");
            products.setPrice("200");
            products.setQuantity("4");
            assertNotNull(repo.save(products));
             
                   }
    
}
