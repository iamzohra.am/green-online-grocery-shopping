package com.example.demo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.example.demo.model.Products;
import com.example.demo.repository.ProductRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace =Replace.NONE)
@Rollback(value=false)
public class ProductRepositoryTests {
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private ProductRepository repo;

	@Test
	public void testCreateProducts1() {
		Products products = new Products();
		products.setProductName("dabur");
		products.setDescription("dabur honey3");
		products.setPrice("252");
		products.setQuantity("3");

		Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	}

	@Test
	public void testCreateProducts2() {
		Products products = new Products();
		products.setProductName("honey");
		products.setDescription("dabur2");
		products.setPrice("256");
		products.setQuantity("1");

		Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	}
		

	
	@Test
	public void testCreateProducts3() {
		Products products = new Products();
		 products.setProductName("SOAP3");
		 products.setDescription("soap3");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	@Test
	public void testCreateProducts4() {
		Products products = new Products();
		 products.setProductName("vegetable");
		 products.setDescription("veg");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	@Test
	public void testCreateProducts5() {
		Products products = new Products();
		 products.setProductName("veg");
		 products.setDescription("vegetable");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	
	
	@Test
	public void testCreateProducts6() {
		Products products = new Products();
		 products.setProductName("fruit");
		 products.setDescription("fruit");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	
	@Test
	public void testCreateProducts7() {
		Products products = new Products();
		 products.setProductName("fruity");
		 products.setDescription("fruity");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	@Test
	public void testCreateProducts8() {
		Products products = new Products();
		 products.setProductName("ice");
		 products.setDescription("ice");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	@Test
	public void testCreateProducts9() {
		Products products = new Products();
		 products.setProductName("icecream");
		 products.setDescription("icecream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	@Test
	public void testCreateProducts10() {
		Products products = new Products();
		 products.setProductName("cream");
		 products.setDescription("cream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	@Test
	public void testCreateProducts11() {
		Products products = new Products();
		 products.setProductName("cream1");
		 products.setDescription("cream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	@Test
	public void testCreateProducts12() {
		Products products = new Products();
		 products.setProductName("icecream");
		 products.setDescription("cream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	
	@Test
	public void testCreateProducts13() {
		Products products = new Products();
		 products.setProductName("icecream1");
		 products.setDescription("cream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	@Test
	public void testCreateProducts14() {
		Products products = new Products();
		 products.setProductName("icecream2");
		 products.setDescription("cream");
		 products.setPrice("254");
		 products.setQuantity("3");
		 Products savedProducts = repo.save(products);

			Products existProducts = entityManager.find(Products.class, savedProducts.getId());

			assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

		
	}
	

@Test
public void testCreateProducts15() {
	Products products = new Products();
	 products.setProductName("icecream3");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts16() {
	Products products = new Products();
	 products.setProductName("icecream4");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts17() {
	Products products = new Products();
	 products.setProductName("icecream5");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts18() {
	Products products = new Products();
	 products.setProductName("icecream6");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts19() {
	Products products = new Products();
	 products.setProductName("icecream7");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts20() {
	Products products = new Products();
	 products.setProductName("icecream8");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}
@Test
public void testCreateProducts21() {
	Products products = new Products();
	 products.setProductName("icecream9");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}

@Test
public void testCreateProducts22() {
	Products products = new Products();
	 products.setProductName("icecream10");
	 products.setDescription("cream");
	 products.setPrice("254");
	 products.setQuantity("3");
	 Products savedProducts = repo.save(products);

		Products existProducts = entityManager.find(Products.class, savedProducts.getId());

		assertThat(products.getProductName()).isEqualTo(existProducts.getProductName());

	
}



}


