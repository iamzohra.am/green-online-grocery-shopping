package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UserRepository repo;

	@Test
	public void testCreateUser1() {
		User user = new User();
		user.setEmail("amna6@gmail.com");
		user.setPassword("fdh");
		user.setFirstName("fhdh");
		user.setLastName("r");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}

	@Test
	public void testCreateUser2() {
		User user = new User();
		user.setEmail("anma6@gmail.com");
		user.setPassword("gh");
		user.setFirstName("gh");
		user.setLastName("x");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	@Test
	public void testCreateUser3() {
		User user = new User();
		user.setEmail("far6@gmail.com");
		user.setPassword("hi");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	
	@Test
	public void testCreateUser4() {
		User user = new User();
		user.setEmail("efs6@gmail.com");
		user.setPassword("n");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	@Test
	public void testCreateUser5() {
		User user = new User();
		user.setEmail("den6@gmail.com");
		user.setPassword("n");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	@Test
	public void testCreateUser6() {
		User user = new User();
		user.setEmail("abr6@gmail.com");
		user.setPassword("n");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	@Test
	public void testCreateUser7() {
		User user = new User();
		user.setEmail("ben6@gmail.com");
		user.setPassword("n");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	@Test
	public void testCreateUser8() {
		User user = new User();
		user.setEmail("cdf6@gmail.com");
		user.setPassword("n");
		user.setFirstName("hi");
		user.setLastName("r1");

		User savedUser = repo.save(user);

		User existUser = entityManager.find(User.class, savedUser.getId());

		assertThat(user.getEmail()).isEqualTo(existUser.getEmail());

	}
	
	  @Test 
	 public void testFindUserByEmail6() {
		 String email = "abr6@gmail.com";
	 User user =repo.findByEmail(email); 
	   assertThat(user).isNotNull();
	 
	 }
	 
}
